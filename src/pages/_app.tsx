import { appWithTranslation } from 'next-i18next';
import { AppProps } from 'next/app';
import Layout from '../app/layout';

const MyApp = ({ Component, pageProps }: AppProps & { pageProps: any }) => (
    <Layout>
        <Component {...pageProps} />
    </Layout>
);

export default appWithTranslation(MyApp);
