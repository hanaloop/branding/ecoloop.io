import lngDetector from '../lib/lngDetector';
import { useRouter } from 'next/router';
import Link from 'next/link';

type Props = {
    locale: string;
    href: string;
};
const LanguageSwitchLink: React.FC<Props> = ({ locale, ...rest }) => {
    const router = useRouter();

    let href = rest.href || router.asPath;
    let pName = router.pathname;

    Object.keys(router.query).forEach((k) => {
        if (k === 'locale') {
            pName = pName.replace(`[${k}]`, locale);
            return;
        }
        // @ts-ignore
        pName = pName.replace(`[${k}]`, router.query[k]);
    });
    if (locale) {
        href = rest.href ? `/${locale}${rest.href}` : pName;
    }

    return (
        <Link href={href} onClick={() => lngDetector?.cache && lngDetector.cache(locale)}>
            {locale}
        </Link>
    );
};

export default LanguageSwitchLink;
