import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import i18nextConfig from '../next-i18next.config';
import { GetStaticPropsContext } from 'next';

export const getI18nPaths = () =>
    i18nextConfig.i18n.locales.map((lng) => ({
        params: {
            locale: lng,
        },
    }));

export const getStaticPaths = () => ({
    fallback: false,
    paths: getI18nPaths(),
});

export async function getI18nProps(ctx: GetStaticPropsContext, ns = ['common']) {
    let locale = ctx?.params?.locale || 'en';
    if (locale instanceof Array) locale = locale[0];
    let props = {
        ...(await serverSideTranslations(locale, ns)),
    };
    return props;
}

export function makeStaticProps(ns = [] as string[]) {
    return async function getStaticProps(ctx: GetStaticPropsContext) {
        return {
            props: await getI18nProps(ctx, ns),
        };
    };
}
